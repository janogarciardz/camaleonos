<?php

Route::group(['middleware' => 'web', 'prefix' => 'inventario', 'namespace' => 'Modules\Inventario\Http\Controllers'], function()
{
    Route::get('/', 'InventarioController@index');
});
