<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>CamaleonOS</title>
		<link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" />

		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Oxygen:300,400,700" rel="stylesheet" type="text/css">

		<!-- Styles -->
		<style>
			html, body {
				background-color: #fff;
				color: #1a1a1a;
				font-family: 'Oxygen', sans-serif;
				font-weight: 100;
				height: 100vh;
				margin: 0;
				
			}

			.full-height {
				height: 100vh;
			}

			.flex-center {
				align-items: center;
				display: flex;
				justify-content: center;
			}

			.position-ref {
				position: relative;
			}

			.top-right {
				position: absolute;
				right: 10px;
				top: 18px;
			}

			.content {
				text-align: center;
			}

			.title {
				font-size: 84px;
				font-weight: lighter;
				color: #1a1a1a;
				text-align: center;
			}

			.title img{
				width: 100%;
				max-width: 150px;
			}

			.title span{
				font-weight: normal;
			}

			.title p{
				margin-top: 0;
			}

			.links > a {
				color: #636b6f;
				padding: 0 25px;
				font-size: 12px;
				font-weight: 600;
				letter-spacing: .1rem;
				text-decoration: none;
				text-transform: uppercase;
			}

			.m-b-md {
				margin-bottom: 30px;
			}

			span.beta{
				font-weight: lighter;
				font-size: 15px;
			}
		</style>
	</head>
	<body>
		<div class="flex-center position-ref full-height">
			@if (Route::has('login'))
				<div class="top-right links">
					@if (Auth::check())
						<a href="{{ url('/home') }}">Escritorio</a>
					@else
						<a href="{{ url('/login') }}">Ingresar</a>
						<a href="{{ url('/register') }}">Registrar</a>
					@endif
				</div>
			@endif

			<div class="content">
				<div class="title m-b-md">
					<img src="{{ asset('img/logo.svg') }}" alt="">
					<p>Camaleon<span>OS</span><span class="beta">Alpha 0.1</span></p>
				</div>

				<div class="links">
					<a href="#">Clientes</a>
					<a href="#">Inventario</a>
					<a href="#">Proveedores</a>
					<a href="#">Proyectos</a>
					<a href="#">...</a>
				</div>
			</div>
		</div>
	</body>
</html>
